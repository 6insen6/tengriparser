﻿using System;

namespace NewsParser.Models
{
    public class News
    {
        public News(string title, string description, DateTime? postDate)
        {
            Title = title;
            Description = description;
            PostDate = postDate;
        }

        public int Id { get; private set; }
        public string Title { get; private set; }    
        public string Description { get; private set; }    
        public DateTime? PostDate { get; private set; }


    }
}
