﻿using Microsoft.EntityFrameworkCore;
using NewsParser.Models;
using System;

namespace NewsParser.Infrastructure
{
    public class ApplicationDbContext : DbContext
    {

        public DbSet<News> News { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
