﻿using Microsoft.AspNetCore.Mvc;
using NewsParser.API.Requests;
using NewsParser.Core.Services.ReaderService;
using NewsParser.Models;
using NewsParser.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace NewsParser.Controllers
{
    [Route("api")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpGet("{count}")]
        public async Task<List<News>> Parse(int count)
        {
            var news = await _newsService.Parse(new TengriParser(), count);
            return news;
            
        }

        [HttpGet]
        [Route("posts")]
        public async Task<IActionResult> GetByDate([FromQuery] DateRequest request) 
        {
            var news = await _newsService.GetByDate(request.From, request.To);

            return Ok(news);        
        }

        [HttpGet]
        [Route("topwords")]
        public async Task<IActionResult> Get([FromQuery] PaginationRequest request) 
        {
            var news = await _newsService.Get(request.Page, request.Size);
            return Ok(news);        
        }

        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> FindByText([FromQuery] string text)
        {
            if (text == null)
                return BadRequest("Не указана строка для поиска");

            var news = await _newsService.SearchByText(text);

            return Ok(news);        
        }
    }
}
