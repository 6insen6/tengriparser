﻿using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;
using System;

namespace NewsParser.API.Configuration
{
    public static class SerilogConfiguration
    {
        public static void UseSerilog()
        {
            string timestamp = DateTime.Now.ToString("yyyyMMdd_HHmm");
            LoggerConfiguration loggerConfiguration = new LoggerConfiguration();

            loggerConfiguration
                .MinimumLevel.Information()
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File(new CompactJsonFormatter(), $"logs/parser-{timestamp}.log");

            Log.Logger = loggerConfiguration.CreateLogger();
        }
    }
}
