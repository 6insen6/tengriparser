﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NewsParser.Infrastructure;

namespace NewsParser.API.Configuration
{
    public static class DatabaseConfiguration
    {
        public static void ConfigureSqlServer(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));
        }
    }
}
