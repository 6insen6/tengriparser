﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NewsParser.Core.Services;
using NewsParser.Core.Services.ReaderService;
using NewsParser.Services;

namespace NewsParser.API.Configuration
{
    public static class StartupConfiguration
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<INewsService, NewsService>();
            services.AddTransient<INewsReader, TengriParser>();
        }

        public static void AddSwagger(this IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "NewsParser v1");
            });
        }
    }
}
