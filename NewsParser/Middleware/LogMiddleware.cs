﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NewsParser.Middleware
{
    public class LogMiddleware
    {
        private readonly RequestDelegate _next;

        public LogMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext context,
            ILogger<LogMiddleware> logger)
        {
            if (!context.Request.Path.StartsWithSegments(new PathString("/api")))
            {
                await _next(context);
            }
            else
            {
                ConvertRequest(context, logger);
                await ConvertResponse(context);
            }
        }

        private static void ConvertRequest(HttpContext context, ILogger logger)
        {
            try
            {
                context.Request.EnableBuffering();
                string queryParams = context.Request.QueryString.Value;

                if (!string.IsNullOrEmpty(queryParams))
                {
                    logger.LogInformation("Request params", queryParams);
                }
            }
            catch
            {
            }
            finally
            {
                context.Request.Body.Seek(0, SeekOrigin.Begin);
            }
        }

        private async Task ConvertResponse(HttpContext context)
        {
            Stream originalResponseBody = context.Response.Body;
            await using MemoryStream memoryStream = new MemoryStream();
            context.Response.Body = memoryStream;

            await _next(context);

            memoryStream.Seek(0, SeekOrigin.Begin);
            string responseBodyContent = await new StreamReader(memoryStream).ReadToEndAsync();
            memoryStream.Seek(0, SeekOrigin.Begin);
            try
            {
                JObject json = JObject.Parse(responseBodyContent);
                StringContent convertedResponseContent = new StringContent(
                    json.ToString(Formatting.None),
                    Encoding.UTF8,
                    "application/json");
                Stream responseBody = await convertedResponseContent.ReadAsStreamAsync();
                context.Response.Headers.ContentLength = responseBody.Length;
                await responseBody.CopyToAsync(originalResponseBody);
            }
            catch
            {
                StringContent originalResponseContent = new StringContent(responseBodyContent);
                Stream responseBody = await originalResponseContent.ReadAsStreamAsync();
                context.Response.Headers.ContentLength = responseBody.Length;
                await responseBody.CopyToAsync(originalResponseBody);
            }
        }
    }

    public static class LogMiddlewareExtensions
    {
        public static IApplicationBuilder UseLogMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LogMiddleware>();
        }
    }
}
