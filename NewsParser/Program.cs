using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NewsParser.API.Configuration;
using Serilog;
using System;

namespace NewsParser
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                //Подключаем серилог
                SerilogConfiguration.UseSerilog();
                Log.Information("Starting web host");
                CreateHostBuilder(args).Build().Run();
                return;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }).UseSerilog();
    }
}
