﻿using System.ComponentModel.DataAnnotations;

namespace NewsParser.API.Requests
{
    public class PaginationRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int Page { get; set; }
        [Required]
        public int Size { get; set; }
    }
}
