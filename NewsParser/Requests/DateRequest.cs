﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace NewsParser.API.Requests
{
    public class DateRequest
    {
        [Required]
        public DateTime From { get; set; }
        [Required]
        public DateTime To { get; set; }
    }
}
