﻿using NewsParser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsParser.Core.Services
{
    public interface INewsReader
    {
        string Url { get; }
        List<News> Parse(int newsCount);
    }
}
