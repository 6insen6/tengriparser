﻿using HtmlAgilityPack;
using NewsParser.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace NewsParser.Core.Services.ReaderService
{
    public class TengriParser : INewsReader
    {
        public string Url { get; }

        public TengriParser()
        {
            Url = "https://tengrinews.kz";
        }

        public List<News> Parse(int newsCount)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument htmlDoc = new HtmlDocument();
            int page = 1;

            List<HtmlNode> nodes = new List<HtmlNode>();

            while (nodes.Count() < newsCount)
            {
                htmlDoc = web.Load(String.Concat(Url, $"/find-out/page/{page}"));

                int takeCount = newsCount - nodes.Count;

                nodes.AddRange(htmlDoc.DocumentNode.SelectNodes("//a[contains(@class,'tn-link')]").Take(takeCount > 20 ? 20 : takeCount));
                page++;
            }

            ConcurrentBag<News> newsBag = new ConcurrentBag<News>();

            Parallel.ForEach(nodes, i =>
            {
                string postLink = i.GetAttributeValue("href", "");
                HtmlDocument document = web.Load(String.Concat(Url, postLink));
                if (document == null)
                {
                    return;
                }
                HtmlNode titleNode = document.DocumentNode.SelectSingleNode(".//h1[contains(@class, 'tn-content-title')]");
                if (titleNode == null)
                {
                    return;
                }
                string dirtyTitle = titleNode.InnerText;

                HtmlNode newsTextNode = document.DocumentNode.SelectSingleNode(".//article[contains(@class, 'tn-news-text')]");

                var trash = newsTextNode.SelectNodes(".//a[@href]").AsEnumerable();

                if (newsTextNode == null)
                {
                    return;
                }
                StringBuilder sb = new StringBuilder();
                sb.Append(newsTextNode.InnerHtml);
                string newsText = newsTextNode.InnerHtml;
                if (trash != null)
                {
                    if (trash != null)
                        trash = trash.Where(x => x.OuterHtml.Contains("https://t.me")).DefaultIfEmpty(null);
                    foreach (var el in trash)
                    {
                        if (el != null)
                            newsText = newsText.Replace(el.OuterHtml, "");
                    }
                }
                newsText = Regex.Replace(newsText, @"( |\t|\r?\n)\1+", String.Empty);
                newsText = Regex.Replace(newsText, @"<[^>]+>|&nbsp;", String.Empty).Trim();
                newsText = HttpUtility.HtmlDecode(newsText);

                string[] splittedTitle = dirtyTitle.Split("\n");
                string title = splittedTitle[0].Trim();
                string postDateString = splittedTitle[1].Trim();

                DateTime? postDate = GetPostDate(postDateString);

                var model = new News(title, newsText, postDate);
                newsBag.Add(model);
            });
            List<News> news = newsBag.ToList();
            return news;
        }

        private DateTime? GetPostDate(string postDateString)
        {
            string month = postDateString.Split(" ")[1];
            if (postDateString.Contains("Вчера"))
            {
                var splittedMonth = month.Split(":");
                return DateTime.Now.AddDays(-1).Date + new TimeSpan(int.Parse(splittedMonth[0]), int.Parse(splittedMonth[1]), 0);
            }
            else
            {
                var monthToReplace = MonthToInt(month);

                if (monthToReplace == null)
                    return null;

                postDateString = postDateString.Replace(month, monthToReplace);
                return DateTime.ParseExact(postDateString, "dd MM yyyy, HH:mm", CultureInfo.InvariantCulture);
            }
        }

        private string MonthToInt(string monthToConvert)
        {
            Dictionary<string, string> months = new Dictionary<string, string>()
            {
                { "января", "01"},
                { "февраля", "02"},
                { "марта", "03"},
                { "апреля", "04"},
                { "мая", "05"},
                { "июня", "06"},
                { "июля", "07"},
                { "августа", "08"},
                { "сентября", "09"},
                { "октября", "10"},
                { "ноября", "11"},
                { "декабря", "12"}
            };
            var month = months.ContainsKey(monthToConvert) ? months[monthToConvert] : null;

            return month;
        }
    }
}
