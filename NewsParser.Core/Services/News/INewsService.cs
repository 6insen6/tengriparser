﻿using NewsParser.Core.Services;
using NewsParser.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsParser.Services
{
    public interface INewsService
    {
        Task<List<News>> Parse(INewsReader reader, int newsCount);
        Task<List<News>> GetByDate(DateTime from, DateTime to);
        Task<List<string>> Get(int page, int size);
        Task<List<News>> SearchByText(string text);
    }
}
