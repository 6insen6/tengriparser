﻿using Microsoft.EntityFrameworkCore;
using NewsParser.Core.Services;
using NewsParser.Infrastructure;
using NewsParser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NewsParser.Services
{
    public class NewsService : INewsService
    {
        private readonly ApplicationDbContext _context;

        public NewsService(ApplicationDbContext context)
        {
            _context = context;
        }

        private IQueryable<News> GetAll()
        {
            return _context.News;
        }
        public async Task<List<News>> GetByDate(DateTime from, DateTime to) => 
            await GetAll().Where(x => x.PostDate > from && x.PostDate < to)
                .AsNoTracking()
                .ToListAsync();

        public async Task<List<News>> Parse(INewsReader reader, int newsCount)
        {
            var news = reader.Parse(newsCount);

            _context.News.AddRange(news);
            await _context.SaveChangesAsync();

            return news;
        }

        public async Task<List<string>> Get(int page, int size)
        {
            var delimiterChars = new char[] { ' ', '.', ',', ':', '\t', '\"', '\r', '{', '}', '[', ']', '=', '/' };

            var news = await GetAll().AsNoTracking().ToListAsync();
            var text = new StringBuilder().AppendJoin("", news.Select(x => x.Description.ToLower())).ToString();
            var cleanedText = Regex.Replace(text, "[^\\p{L}]", " ");

            var dict = cleanedText
                .Split(" ")
                .Where(x => x.Length >= 4)
                .GroupBy(x => x)
                .Select(x => new {
                    Word = x.Key,
                    Count = x.Count()
                }).OrderByDescending(x => x.Count).ThenBy(x => x.Word).Skip((page - 1) * size).Take(size)
            .ToDictionary(x => x.Word, y => y.Count);

            var words = dict.Select(x => x.Key).ToList();

            return words;
        }

        public async Task<List<News>> SearchByText(string text) => 
            await GetAll().AsNoTracking().Where(x => x.Description.Contains(text)).ToListAsync();
    }
}
